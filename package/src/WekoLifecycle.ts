import type {
  WekoChangedEventAttributeDetails,
  WekoChangedEventDetails,
  WekoLifecycleType
} from "./types.ts";
import { WEKO_CHANGED_EVENT } from "./shared.ts";

/**
 * An interface implemented by classes that can be used as the base class for the
 * {@link WekoLifecycleMixin}.
 *
 * @privateRemarks
 * The Constructor type definition must be in this file for typedoc to generate the right generic
 * information.
 */
export interface Constructor<T = HTMLElement> {
  new (...input: any[]): T;
}

/**
 * A factory function that generates a class constructor for a web component derived from
 * `HTMLElement` that includes {@link WekoLifecycleType} functionality.
 *
 * @returns A class constructor that includes the `baseType` functionality and implements
 * {@link WekoLifecycleType}.
 *
 * Class instances emit the `weko-changed-event` ({@link WEKO_CHANGED_EVENT}), a CustomEvent with
 * detail information in the shape of {@link WekoChangedEventDetails}.
 *
 * @remarks
 * Use this mixin when you want to include weko lifecycle capabilities in a web component. It may be
 * combined with other mixins.
 *
 * @example class extends WekoLifecycleMixin
 * ```ts
 * class MyWebComponent extends WekoLifecycleMixin(HTMLElement) {}
 * ```
 *
 * @example extending with multiple weko mixins
 * ```ts
 * class MyWebComponent extends WekoRoutingMixin(WekoLifecycleMixin(HTMLElement)) {}
 * ```
 *
 * @param baseType - A class constructor for a type that implements the {@link !HTMLElement}
 * interface.
 * @template T - A type of object that implements the {@link !HTMLElement} interface.
 */
export function WekoLifecycleMixin<T extends Constructor>(
  baseType: T
): Constructor<WekoLifecycleType> & T {
  return class WekoLifecycle extends baseType implements WekoLifecycleType {
    #attributeBuffer: WekoChangedEventAttributeDetails["attributes"] | undefined;
    #attributeTaskQueued = false;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    constructor(...args: any[]) {
      super(...args);
    }

    /*
     * Web component lifecycle API
     */
    // #region

    attributeChangedCallback(name: string, oldValue: unknown, newValue: unknown): void {
      // @ts-ignore ...annoying
      super.attributeChangedCallback &&
        // @ts-ignore ...annoying
        super.attributeChangedCallback(name, oldValue, newValue);

      this.#attributeBuffer = this.#attributeBuffer || {};
      this.#attributeBuffer[name] = { newValue, oldValue };

      if (!this.#attributeTaskQueued) {
        this.#attributeTaskQueued = true;

        queueMicrotask(() => {
          this.#attributeTaskQueued = false;

          if (!this.#attributeBuffer) {
            return;
          }

          const attributes = this.#attributeBuffer;
          this.#attributeBuffer = undefined;

          this.#dispatchEvent({
            attributes,
            type: "attributes"
          });
        });
      }
    }

    connectedCallback(): void {
      // @ts-ignore ...annoying
      super.connectedCallback && super.connectedCallback();
      this.#dispatchEvent({ type: "connected" });
    }

    disconnectedCallback(): void {
      // @ts-ignore ...annoying
      super.disconnected && super.disconnected();
      this.#dispatchEvent({ type: "disconnected" });
    }

    // #endregion

    #dispatchEvent<D extends WekoChangedEventDetails>(data: D): void {
      const eventInitDict: CustomEventInit<D> = { detail: data };
      this.dispatchEvent(new CustomEvent<D>(WEKO_CHANGED_EVENT, eventInitDict));
    }
  };
}
