/**
 * This files exists only to generate TypeDoc documentation. It is not part of the build or
 * deployed.
 */
export * from "./index.ts";
import type { Constructor } from "./WekoLifecycle";

export type { Constructor };
