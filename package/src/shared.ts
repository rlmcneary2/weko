import type { WekoChangedEventAttributeDetails, WekoChangedEventDetails } from "./types";

/**
 * The name of the {@link !CustomEvent} emitted when something interesting has happened.
 *
 * @remarks
 * See {@link WekoChangedEventAttributeDetails} for information about this event's details.
 */
export const WEKO_CHANGED_EVENT = "weko-changed-event";

/**
 * A [type
 * predicate](https://www.typescriptlang.org/docs/handbook/2/narrowing.html#using-type-predicates)
 * to narrow an `obj` (usually an {@link !Event} instance) to a {@link !CustomEvent} with a
 * {@link WekoChangedEventDetails} detail.
 * @param obj The object to be tested
 * @returns True if `obj` has a `detail.type`.
 */
export function isWekoChangedEvent(obj: any): obj is CustomEvent<WekoChangedEventDetails> {
  return obj && "detail" in obj && "type" in obj.detail;
}

/**
 * A [type
 * predicate](https://www.typescriptlang.org/docs/handbook/2/narrowing.html#using-type-predicates)
 * to narrow an `obj` (usually an {@link !Event} instance) to a {@link !CustomEvent} with a
 * {@link WekoChangedEventAttributeDetails} detail.
 * @param obj The object to be tested
 * @returns True if `obj` has a `detail.type` that equals "attributes."
 */
export function isWekoChangedEventAttribute(
  obj: any
): obj is CustomEvent<WekoChangedEventAttributeDetails> {
  return isWekoChangedEvent(obj) && obj.detail.type === "attributes";
}
